Source: python-ewmh
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Reuben Thomas <rrt@sc3d.org>, Andrej Shadura <andrewsh@debian.org>
Build-Depends: debhelper (>= 9),
               dh-python,
               python3-all,
               python3-setuptools (>= 0.6.24),
               python3-sphinx,
               python3-xlib
Standards-Version: 3.9.8
Homepage: https://github.com/parkouss/pyewmh/
Vcs-Git: https://salsa.debian.org/python-team/modules/python-ewmh.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/python-ewmh

Package: python3-ewmh
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Suggests: python-ewmh-doc
Description: Python interface to EWMH-compliant window managers (Python 3)
 python-ewmh is a 100% pure Python implementation of the EWMH (Extended
 Window Manager Hints) protocol, which can be used to query and control
 EWMH-compliant window managers.
 .
 This package installs the library for Python 3.

Package: python-ewmh-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: Python interface to EWMH-compliant window managers (common documentation)
 python-ewmh is a 100% pure Python implementation of the EWMH (Extended
 Window Manager Hints) protocol, which can be used to query and control
 EWMH-compliant window managers.
 .
 This is the common documentation package.
